# AskNicely Testimonials #

Imports your AskNicely testimonials into your WordPress install.  Testimonials are imported as a custom post type.

A widget is available to display testimonials.

You can override the default template by creating a copy of testimonials-template.php in your theme directory. 


### Installation ###

composer require asknicely/testimonials