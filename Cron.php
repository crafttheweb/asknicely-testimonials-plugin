<?php
namespace AskNicely;

use \AskNicely\Import;

class Cron {

    const EVENT_NAME = 'asknicely_import';

    static function init() {

        add_action(self::EVENT_NAME, function(){
            Import::import();
        });
    }

    static function schedule($force=false) {

        $timestamp = wp_next_scheduled(SELF::EVENT_NAME);

        if(!$timestamp || $force) {
            wp_schedule_event( time(), 'twicedaily', self::EVENT_NAME );
        }
    }
}
