<?php

namespace AskNicely;

use \AskNicely\TEXT_DOMAIN;

class Widget extends \WP_Widget {

    const SORT_TIME = 'recent';
    const SORT_RANDOM = 'random';
    const LIMIT = 5;
    const TEMPLATE_NAME = 'testimonials-template.php';

    static function init() {
        add_action( 'widgets_init', function(){
            register_widget( get_called_class() );
        });
    }

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'asknicely_widget', // Base ID
            __('AskNicely Testimonials', TEXT_DOMAIN ), // Name
            ['description' => __('Display your AskNicely testimonials.', TEXT_DOMAIN ), ] // Args
        );
    }

    function widget( $args=[], $instance=[] ) {

        $params = $args + (array) $instance + [
            'template_args' => [],
            'post_args' => []
        ];

        $sorts = [
            self::SORT_TIME => [
                'orderby'=>'date',
                'order' => 'DESC'
            ],
            self::SORT_RANDOM => [
                'orderby' => 'rand'
            ]
        ];

        $posts_args = $params['post_args'] + [
            'posts_per_page' => $params['limit'],
            'post_type' => Testimonial::POST_TYPE,
        ];
        

        if(isset($sorts[$params['sort']])){
            $posts_args += $sorts[$params['sort']];
        }

        $template_args = $params['template_args'] + [
            'wrapper_css_class' => 'testimonials',
            'css_class' => 'testimonial',
            'image_size' => 'thumbnail',
            'testimonials' => get_posts(apply_filters('ask_nicely_widget_get_posts',$posts_args,$params))
        ];

        $theme_path = get_stylesheet_directory().'/'.self::TEMPLATE_NAME;
        $plugin_path = __DIR__.'/'.self::TEMPLATE_NAME;
        $render = function ($path) use ($template_args) {
            extract($template_args);
            include($path);
        };

        if(file_exists($theme_path)){
            $render($theme_path);   
        } else {
            $render($plugin_path);
        }
    }

    /**
     * Back-end widget form.
     * @param array $instance Previously saved values from database.
     */
    function form( $instance ) {

        $defaults = [
            'title' => __( 'Testimonials', TEXT_DOMAIN ),
            'sort' => self::SORT_TIME,
            'limit' => self::LIMIT,
        ];

        $instance = (array) $instance + $defaults;

        extract($instance); ?>
        
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'sort' ); ?>"><?php _e( 'Sort By:' ); ?></label> 
            <select class="widefat" id="<?php echo $this->get_field_id( 'sort' ); ?>" name="<?php echo $this->get_field_name( 'sort' ); ?>">
                <?php foreach($this->sort_options() as $value => $text):?>
                    <option value="<?php echo $value?>" <?php selected($value, $sort)?> ><?php echo $text?></option>
                <?php endforeach;?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e( 'Limit:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="number" value="<?php echo (int) $limit; ?>">
        </p>
        
<?php 
    }

    /**
     * Sanitize widget form values as they are saved.
     */
    function update( $new_values, $old_values ) {

        $validate = [
            'title' => 'strip_tags',
            'limit' => 'intval',
            'sort' => function($value){
                return in_array($value, array_keys($this->sort_options())) ? $value : self::SORT_TIME;
            }
        ];

        return array_reduce(array_keys($new_values), function($safe, $field) use ($new_values, $validate){

            $safe[$field] = $validate[$field]($new_values[$field]);

            return $safe;

        },[]);
    }
    
    protected function sort_options(){
        return [
            self::SORT_TIME => __('Most Recent', TEXT_DOMAIN),
            self::SORT_RANDOM => __('Random', TEXT_DOMAIN)
        ];
    }

}
