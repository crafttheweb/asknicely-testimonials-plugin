<?php

namespace AskNicely;

use \AskNicely\TEXT_DOMAIN;

class AdminSettings extends \TheFold\WordPress\Admin {

    const SETTINGS = 'asknicely-settings';
    const CLIENT_ID = 'client_id';
    protected $section_title = 'Setup';


    function get_page_name() {
        return  __('AskNicely', TEXT_DOMAIN );
    }

    function init()
    {
        //Show setup link in plugins admin
        add_filter('plugin_action_links_asknicely/plugin.php', function($links){

            $links[] = sprintf('<a href="%s">Setup</a>',self::get_href());

            return $links;
        });

        //Show setup notice
        add_action('admin_notices', function(){

            $client_id = self::get_client_id();
            $ignore = isset($_GET['page']) && $_GET['page'] == self::SETTINGS;

            if(empty($client_id) && !$ignore){
?>
        <div class="updated"><p><?php echo sprintf(__('Setup your AskNicely testimonials <a href="%s">here</a>'), self::get_href())?></p></div>
<?php 
            }
        });

        //Import testimonials again when options change
        add_action('update_option_'.self::SETTINGS, function($old_value, $new_value){
            Cron::schedule(true);
        },10,2);

        $this->add_field(self::CLIENT_ID,'Account ID <br/><small>Pleace enter your AskNicely Account ID</small>');

        $this->render();
    }
 
    function get_settings_namespace()
    {
        return self::SETTINGS;
    }

    static function get_client_id()
    {
        return \TheFold\WordPress::get_option(self::SETTINGS,self::CLIENT_ID);
    }

    static function get_href()
    {
        return admin_url('options-general.php?page='.self::SETTINGS);
    }

}
