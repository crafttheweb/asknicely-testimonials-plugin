<?php
/**
 * Plugin Name: AskNicely Testimonials
 * Plugin URI: http://www.asknice.ly/
 * Description: Integrate AskNicely testimonials with your site.
 * Version: 1.0.0
 * Author: The Fold
 * Author URI: http://www.thefold.co.nz/
 * License: GPL2
 */
namespace AskNicely;

require __DIR__.'/autoload.php';

const TEXT_DOMAIN = 'asknicely';

Testimonial::init();
Cron::init();
Widget::init();
Shortcode::init();

if(is_admin()){
    $Admin = new AdminSettings();
    $Admin->init();
}

register_activation_hook( __FILE__, function(){
    Cron::schedule();
});

register_deactivation_hook( __FILE__, function (){
    wp_clear_scheduled_hook( Cron::EVENT_NAME );
});

