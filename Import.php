<?php
namespace AskNicely;

use \TheFold\WordPress\Import as Importer;
use \AskNicely\Testimonial;

class Import {

    const JSON_FILE = 'http://static.asknice.ly/published/asknicely/recommend_all.json';

    static function parse_file(){

        if($contents = file_get_contents(self::JSON_FILE)){
            return json_decode($contents,true);
        }
    }

    static function post_mapping(){

        return [
            'ID' => function($data){
                global $wpdb;

                $ID = $wpdb->get_var( $wpdb->prepare(
                    "SELECT ID FROM 
                    $wpdb->posts AS p
                    JOIN $wpdb->postmeta AS m ON m.post_id = p.ID AND m.meta_key = 'integration_id'
                    WHERE m.meta_value = %s AND p.post_type = %s",
                    $data['id'], Testimonial::POST_TYPE ));

                return $ID;
            },
            'integration_id' => 'id',
            'post_content' => 'comment',
            'post_title' => 'name',
            'avatar_url' => 'avatar',
            'post_date' => function($data){
                return date('Y-m-d H:i:s',$data['time']);
            }
        ];
    }

    static function import() {

        $mapping = static::post_mapping();
        $file_data = static::parse_file();
        $imported = [];

        foreach($file_data as $testimonial){

            $post_data = Importer::map_data($mapping, $testimonial);
            $ID = Importer::import_post($post_data,\AskNicely\Testimonial::POST_TYPE,'publish');

            if($ID && $attach_id = Importer::create_attachment([
                'path'=>$post_data['avatar_url'],
                'parent_post_id' => $ID
            ])) { 

                set_post_thumbnail($ID, $attach_id);
            }

            $imported[] = $ID;
        }

        return $imported;
    }
}
