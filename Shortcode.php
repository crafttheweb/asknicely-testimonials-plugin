<?php

namespace AskNicely;

class Shortcode {

    const SHORTCODE = 'asknicely-testimonials';

    function init() {

        add_shortcode( self::SHORTCODE , function($attrs){

            $args = shortcode_atts( [
                'limit' => Widget::LIMIT,
                'sort' => Widget::SORT_TIME,
            ], $attrs, self::SHORTCODE );

            $TestimonialWidget = new Widget();

            ob_start();
            $TestimonialWidget->widget($args);
            return ob_get_clean();
        });
    }
}
