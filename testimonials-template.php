<div class="<?php echo $wrapper_css_class?>">
<?php foreach($testimonials as $testimonial): ?>
    <blockquote <?php echo $css_class?>>
    <?php echo get_the_post_thumbnail( $testimonial->ID, $image_size ); ?>
    <?php echo apply_filters( 'the_content', $testimonial->post_content ); ?>
    <footer><cite><?php echo apply_filters( 'the_title', $testimonial->post_title, $page->ID ); ?></cite></footer>
    </blockquote>
<?php endforeach; ?>
</div>
