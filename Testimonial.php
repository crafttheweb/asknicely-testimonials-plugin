<?php
namespace AskNicely;

use \TheFold\WordPress\CustomPostType;

class Testimonial {
    
    const POST_TYPE = 'asknicelytestimonial';

    static function init(){
        
        new CustomPostType(self::POST_TYPE,'Testimonial',['menu_icon'=>'dashicons-awards']);

        self::hide_testimonial_attachements(); 
    }

    /**
     * Hide testimonail attachments from the media library 
     */
    static function hide_testimonial_attachements()
    {
        add_filter('ajax_query_attachments_args', function($query) {

            add_filter('posts_join', function ($join) {
                global $wpdb;
                $join .= " LEFT JOIN {$wpdb->posts} as asknicely_post_parent ON ({$wpdb->posts}.post_parent = asknicely_post_parent.ID) ";
                return $join;
            });

            add_filter('posts_where', function ($where) {

                global $wpdb;

                if (isset($_POST['post_id'])) {
                    //Don't filter out testimonail media if we are working with a testimonial
                    $post = get_post($_POST['post_id']);
                    if($post->post_type == self::POST_TYPE){
                        return $where;
                    }
                }

                //Don't show testimonials uploaded
                $where .= $wpdb->prepare(" AND ( asknicely_post_parent.post_type IS NULL OR  asknicely_post_parent.post_type != %s ) ", self::POST_TYPE);

                return $where;
            });

            return $query;
        });
    }
}
